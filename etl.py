import pyodbc, os, datetime

def get_tab_conn():
	return pyodbc.connect(os.environ.get('TABPSQL'))

def get_datamart_conn():
	return pyodbc.connect(os.environ.get('SQL_SERVER_CREDENTIALS'))


q = """
select he.id, het.name as event_name
     , hu_actor.name as actor_name,created_at, he.hist_view_id
, hv.name as ViewName
         from historical_events he
     join historical_event_types het on he.historical_event_type_id = het.type_id
     left join hist_users hu on hu.id = he.hist_target_user_id
     left join hist_users hu_actor on hu_actor.id = he.hist_actor_user_id
     left join hist_views hv on hv.id = he.hist_view_id
where het.type_id in (84,1) and he.created_at >= ?
order by created_at desc;
"""

def get_data():
     conn = get_tab_conn()
     c = conn.cursor()
     c.execute(q, (datetime.datetime.utcnow() - datetime.timedelta(days=10)).strftime('%Y-%m-%d') )
     out = c.fetchall()
     return out


def insert_data(data):
     conn = get_datamart_conn()
     insert_q = """
     INSERT INTO tableauActivity(id, event_name, actor_name, created_at, hist_view_id, viewname)  
     values(?,?,?,?,?,?)
     """
     c = conn.cursor()
     c.fast_executemany = True
     c.executemany(insert_q, data)
     c.commit()
     return None