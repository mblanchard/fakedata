from flask import Flask, jsonify, request
from flask_cors import CORS
import os, pyodbc
import datetime, pandas, sqlite3
import threading, time

testiness = False
def init_dataframe():
	if testiness:
		conn = sqlite3.connect('data.sqlite')
		c = conn.cursor()
		c.execute("""SELECT id, event_name, actor_name
		     , cast(created_at as varchar) created_at
		     , hist_view_id, viewname
				 from tableauActivity 
						where created_at >= ? and created_at < ?""",
				  (
					  (datetime.datetime.utcnow() - datetime.timedelta(
						  days=2)).strftime('%Y-%m-%d %H:%M:%S'),
					  datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
				  )
				  )
	else:
		conn = pyodbc.connect(os.environ.get('TABLEAU_PG_ODBC_CONNSTRING'))
		c = conn.cursor()
		c.execute("""
						select he.id, het.name as event_name
						, lower(hu_actor.name) as actor_name,created_at, he.hist_view_id
						, hv.name as ViewName
						from historical_events he
						join historical_event_types het on he.historical_event_type_id = het.type_id
						left join hist_users hu on hu.id = he.hist_target_user_id
						left join hist_users hu_actor on hu_actor.id = he.hist_actor_user_id
						left join hist_views hv on hv.id = he.hist_view_id
						where het.type_id in (84,1) and he.created_at >= ?
						order by created_at desc;""",
				(
						(datetime.datetime.utcnow() - datetime.timedelta(
						days=2)).strftime('%Y-%m-%d %H:%M:%S')
						)
				)

	out = c.fetchall()
	df = pandas.DataFrame([list(x) for x in out], columns = [x[0] for x in c.description])
	return df

def get_users():
	if testiness:
		return pandas.DataFrame()
	conn = pyodbc.connect(os.environ.get('SQL_SERVER_CREDENTIALS'))
	c = conn.cursor()
	c.execute("""
	select lower(RHC.USER_ID)                                          as user_id,
		   concat(
				   case
					   when RHC.PREFERRED_NAME is null or RHC.PREFERRED_NAME = '' then RHC.FIRST_NAME
					   else RHC.PREFERRED_NAME end
			   , ' ', RHC.LAST_NAME
		   )                                                           as personName,
		   RHC.TGRAD_GEOGRAPHY                                         as geography,
		   RHC.TGRAD_REGION                                            as region,
		   case when vips.user_id is null then 'False' else 'True' end as vip,
		   RHC.DEPARTMENT_TREE                                         as department
	from RptHeadCount RHC
			 left join (select lower(user_id) user_id
						from (select distinct l2.user_Id
							  from RptHeadCount RHC
									   join RptHeadCount l2 on RHC.LEVEL_2_MANAGER_NUMBER = l2.PERSON_NUMBER
							  union all
							  select distinct l3.user_Id
							  from RptHeadCount RHC
									   join RptHeadCount l3 on RHC.LEVEL_3_MANAGER_NUMBER = l3.PERSON_NUMBER
							  union all
							  select distinct username
							  from TEMP_MATT_VIPS TMV) a) vips on rhc.user_id = vips.user_id
		""")

	df = pandas.DataFrame([list(x) for x in c.fetchall()], columns = [x[0] for x in c.description])

	return df

def update_dataframe():
	global df
	global vips
	timestamp_filter = df.created_at.max()
	if testiness:
		conn = sqlite3.connect('data.sqlite')
		c = conn.cursor()
		c.execute("""SELECT id, event_name, actor_name
	     , cast(created_at as varchar) created_at
	     , hist_view_id, viewname
				from tableauActivity 
					where created_at <= ? and created_at > ?""", (
			datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'), timestamp_filter))
		new_data = pandas.DataFrame([list(x) for x in c.fetchall()],
									columns=[x[0] for x in c.description])

	else:
		conn = pyodbc.connect(os.environ.get('TABLEAU_PG_ODBC_CONNSTRING'))
		c = conn.cursor()
		c.execute("""select he.id, het.name as event_name
						, lower(hu_actor.name) as actor_name,created_at, he.hist_view_id
						, hv.name as ViewName
						from historical_events he
						join historical_event_types het on he.historical_event_type_id = het.type_id
						left join hist_users hu on hu.id = he.hist_target_user_id
						left join hist_users hu_actor on hu_actor.id = he.hist_actor_user_id
						left join hist_views hv on hv.id = he.hist_view_id
						where het.type_id in (84,1) and he.created_at > ?
						order by created_at desc;""", (timestamp_filter))

	new_data = pandas.DataFrame([list(x) for x in c.fetchall()],
									columns=[x[0] for x in c.description])
	if not new_data.empty:
		df = pandas.concat([df, new_data])
	return None

def update_dataframe_asynchronously():
	global df
	while True:
		print('Updating dataframe, max_timestamp: %s' % df.created_at.max())
		update_dataframe()  # The existing function to update the DataFrame
		time.sleep(10)  # Wait for 5 seconds before updating again



# instantiate the app
app = Flask(__name__)
app.config.from_object('config.MyConfig')
CORS(app, resources={r'/*': {'origins': 'http://localhost:8080'}})

users = get_users()
df = init_dataframe()


update_thread = threading.Thread(target=update_dataframe_asynchronously)
update_thread.daemon = True  # This will ensure the thread exits when the main thread does
update_thread.start()


@app.route('/recentlogins', methods=['GET'])
def recentlogins():
	global df
	global users
	l30m = (datetime.datetime.utcnow() - datetime.timedelta(minutes=60)).strftime('%Y-%m-%d %H:%M:%S')
	today_timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%d 00:00:00')
	recent_logins = \
	df[['actor_name', 'created_at']][
		(df.event_name == 'Login') & (df.actor_name != 'tableauread')].merge(users[['user_id', 'vip']], left_on='actor_name', right_on='user_id').sort_values('created_at', ascending=False).head(
		20).to_dict(orient='records')
	top_views = df[(df.event_name=='Access View') & (df.created_at>l30m)].groupby('viewname').size().reset_index().sort_values(0,
				ascending=False).head(20).rename(columns={0:'number_of_views'}).to_dict(orient='records')
	num_logins = df[df.created_at>l30m].actor_name.nunique()

	vips = df.merge(users[['user_id', 'vip']], left_on='actor_name', right_on='user_id')
	vip_recent_logins = \
	vips[(vips.vip == 'True') & (vips.actor_name != 'mblanchard')][['actor_name','created_at']].groupby('actor_name').max().reset_index().sort_values('created_at',ascending=False
																																					   ).head(
		30).to_dict(orient='records')

	return jsonify({'recent_logins': recent_logins,
					'num_logins': num_logins,
					'top_views': top_views,
					'vip_activity': vip_recent_logins
					})

@app.route('/getuser', methods=['GET'])
def getUserDetails():
	username = request.args.get('username',None)
	global df
	recent_activity = df[df.actor_name == username].sort_values('created_at',ascending=False).fillna(' ').head(20).to_dict(orient='records')
	return jsonify({'userActivity':recent_activity})

@app.route('/getview', methods=['GET'])
def getViewDetails():
	viewname = request.args.get('viewname',None)
	global users
	global df
	recent_activity = df[df.viewname == viewname].merge(users,left_on='actor_name',right_on='user_id').sort_values('created_at', ascending=False).fillna(' ').head(40).to_dict(orient='records')
	return jsonify({'viewActivity': recent_activity})


if __name__ == '__main__':
	app.run()


## Test different column arrangements to see if they can be serialized

# def yday(df):
# 	yd = df[df.created_at >= (datetime.datetime.utcnow() - datetime.timedelta(hours=24)).strftime('%Y-%m-%d')]
# 	yd['dt'] = yd.created_at.apply(lambda x: x[:10])
# 	yd['tm'] = yd.created_at.apply(lambda x: x[-8:-3])
# 	yd.groupby(['dt', 'tm']).size().reset_index().pivot('tm', 'dt', 0).fillna(0).astype(int).cumsum().to_dict(
# 		orient='series')
# 	return df
#
#
#
# def tst(df):
# 	l30m = (datetime.datetime.utcnow() - datetime.timedelta(minutes=30)).strftime('%Y-%m-%d %H:%M:%S')
# 	today_timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%d 00:00:00')
# 	recent_logins = df[['actor_name','created_at']][df.event_name=='Login'].sort_values('created_at',ascending=False).head(20).to_dict(orient='records')
# 	top_views = df[(df.event_name=='Access View') & (df.created_at>l30m)].groupby('viewname').size().reset_index().sort_values(0,ascending=False).head(10).rename(columns={0:'number_of_views'}).to_dict(orient='records')
# 	return df