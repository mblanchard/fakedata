import json, datetime, numpy, pandas
import decimal

class DateEncoding(json.JSONEncoder):
    # known bug pylint bug below [https://github.com/PyCQA/pylint/issues/414]
    # pylint: disable=method-hidden
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')

        if isinstance(obj, numpy.datetime64):
            return str(obj)

        if isinstance(obj, datetime.date):
            return obj.__str__()

        if isinstance(obj, decimal.Decimal):
            return str(obj)

        if isinstance(obj, numpy.floating):
            return float(obj)

        if isinstance(obj, pandas._libs.tslibs.timestamps.Timestamp):
            return str(obj)

        return json.JSONEncoder.default(self, obj)


class MyConfig(object):
    RESTFUL_JSON = {'cls': DateEncoding}

